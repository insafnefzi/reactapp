import React, { Component} from 'react';
import './App.css';
import Person from './Person/Person.js';


class App extends Component {
 state={
  persons:[
    {name: 'max', age:28},
    {name: 'manu', age:28},
    {name: 'steph', age:28}
  ],
  otherState:'some  other value'
}
//const [otherState, setOtherState]=useState('some othr val');
//console.log(otherState,setOtherState);
 switchNameHandler = (newnAME)=>{
  //console.log('Was clicked');
// dont do this  this.state.persons[0].name='Maximilien';
this.setState({
  persons:[
    {name: newnAME, age:28},
    {name: 'manu', age:28},
    {name: 'steph', age:25}
]
})
 }
nameChangeHandler = (event) => {
  this.setState({
    persons:[
      {name: 'Max', age:28},
      {name: event.target.value, age:28},
      {name: 'steph', age:25}
  ]
  })
}
render() {
  const style={
    backgroundColor:'white',
    font :'inherit',
    border:'1px solid blue ',
    padding:'8px',
    cursor: 'pointer'
  };
     return (
      <div className="App">
          <h1 className="App-title">Hi I am react app</h1>
          <button style={style}
           onClick={() =>this.switchNameHandler('max!!')}> Switch name</button>
        <Person 
        name ={this.state.persons[0].name} 
        age={this.state.persons[0].age} />
        <Person 
        name ={this.state.persons[1].name} 
        age={this.state.persons[1].age}
        changed={this.nameChangeHandler}/>
        <Person 
        name ={this.state.persons[2].name} 
        age={this.state.persons[2].age}
        click={this.switchNameHandler.bind(this,'Maximil!!')}
       >My Hobbies: kick ass </Person>
      </div>
    ); 
   // return React.createElement('div',{className: 'App'},React.createElement('h1', null, 'Does this work now'));
  
}
}

export default App;

