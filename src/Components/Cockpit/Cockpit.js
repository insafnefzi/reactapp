import React,{ useEffect, useRef} from 'react';
import classes from './Cockpit.css';
import AuthContext from '../../context/auth-context';
const cockpit = props => {
const toggleBtnRef = useRef(null);


    useEffect ( () => {
      console.log('[Cockpit.js] useEffects');
      // const timer = setTimeout(()=>{
      //   alert('saved data to cloud! ');
      // }, 1000);
      toggleBtnRef.current.click();
      return () => {
         //clearTimeout(timer);
        console.log('[Cockpit.js] cleanup work in useEffects');
      };
    }, []);

    useEffect (()=> {
      console.log('[Cockpit.js] 2nd useEffects');
      return () => {
        console.log('[Cockpit.js] cleanup work in 2nd useEffects');
      };
    });
  
    
    const assignedClasses = [];
    
if (props.personsLength <= 2){
  assignedClasses.push(classes.red);
}
if (props.personsLength <= 1){
  assignedClasses.push(classes.bold);
}
let btnClass = " "; 
    if (props.showPersons){
        btnClass=classes.Red ;
    } 
    return (
        <div className={classes.Cockpit} >
        <h1 >{props.title}</h1>
        <p className={assignedClasses.join(' ')}> this is really working</p>
        <button ref={toggleBtnRef} className="{btnClass}"
         onClick={props.clicked}> Toggle Persons</button>
         <AuthContext.Consumer>
           { context =>  <button onClick={context.login} >Log in</button> }
          
           </AuthContext.Consumer> 
        </div>
         
    );
};

export default React.memo(cockpit);